﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication3.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data;

namespace WebApplication3.Repository
{
    public class RecordRepository : IRecordRepository
    {
        string connectionString = "";
        public RecordRepository()
        {
            connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Records;Integrated Security=True";
        }
        public List<RecordViewModel> GetAllRecords()
        {
            using (SqlConnection connection= new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDependency.Start(connectionString);

                string cmdText = "exec MY_EntityRecordsList_Master";
                var cmd = new SqlCommand(cmdText, connection);
                SqlDependency dependency = new SqlDependency(cmd);
                dependency.OnChange += new OnChangeEventHandler(EntityRecordsListChangeNotification);
                SqlDataReader reader = cmd.ExecuteReader();
                return GetList<RecordViewModel>(reader);
            }
        }
        private void EntityRecordsListChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            var notification = e.ToString();
        }
        private List<T> GetList<T>(IDataReader reader)
        {
            var list = new List<T>();
            while (reader.Read())
            {
                var type = typeof(T);
                T obj = (T)Activator.CreateInstance(type);
                var columnIndex = 0;
                foreach (var prop in type.GetProperties())
                {
                    var propType = prop.Name != "EndDate" ? prop.PropertyType : typeof(DateTime);
                    try
                    {
                        var check = reader[columnIndex].ToString();
                        prop.SetValue(obj, Convert.ChangeType(reader[columnIndex].ToString(), propType));
                    }
                    catch { }
                    columnIndex++;
                }
                list.Add(obj);
            }
            return list;
        }
    }
}