﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using WebApplication3.Models;

namespace WebApplication3.Repository
{
    public interface IRecordRepository
    {
        List<RecordViewModel> GetAllRecords();
    }
}