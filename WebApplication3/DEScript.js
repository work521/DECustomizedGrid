﻿$(() => {
    var deferredCountries = $.Deferred();
    var deferredProjects = $.Deferred();
    var deferredRecords = $.Deferred();

    WebApplication3.WebService.GetData("Countries", result => deferredCountries.resolve(JSON.parse(result)));
    WebApplication3.WebService.GetData("Projects", result => deferredProjects.resolve(JSON.parse(result)));
    WebApplication3.WebService.GetData("Records", result => deferredRecords.resolve(JSON.parse(result)));
    $.when(deferredCountries, deferredProjects, deferredRecords).done(function (countries, projects, records) {
        console.log(records);
        console.log(countries);
        console.log(projects);

        $('#RecordsGrid').dxDataGrid({
            dataSource: records,
            keyExpr: 'Id',
            scrolling: {
                rowRenderingMode: 'virtual',
            },
            paging: {
                pageSize: 10,
            },
            pager: {
                visible: true,
                allowedPageSizes: [5, 10, 20, 'all'],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            editing: {
                mode: 'popup',
                allowUpdating: true,
                allowDeleting: true,
                allowAdding: true,
                popup: {
                    title: 'Record Info',
                    showTitle: true,
                    width: 700,
                },
                form: {
                    items: [{
                        itemType: 'group',
                        colCount: 2,
                        colSpan: 2,
                        items: ['Name', 'CountryId', 'ProjectId', 'StartDate', 'EndDate'],
                    },
                    {
                        dataField: 'Description',
                        editorType: 'dxTextArea',
                        colSpan: 2,
                        editorOptions: {
                            height: 100,
                        },
                    }
                    ],
                },
            },
            onRowPrepared: function (e) {
                if (e.rowType == "data") {
                    if (new Date(Date.parse(e.data.StartDate)) > new Date())
                        e.rowElement.css("background-color", "#f0ffff");
                    else {
                        if (e.data.EndDate != null)
                            e.rowElement.css("background-color",
                                new Date(Date.parse(e.data.EndDate)) < new Date() ?
                                    "#faecd7" :
                                    "#fdf6e7");
                        else
                            e.rowElement.css("background-color", "#fdf6e7");
                    }
                }
            },
            focusedRowEnabled: true,
            focusedRowKey: 117,
            showBorders: true,
            columnAutoWidth: true,
            showRowLines: true,
            columns: [{
                dataField: 'Name',
                validationRules: [{ type: 'required' }],
            }, {
                dataField: 'Description',
                visible: false,
            }, {

                dataField: 'CountryId',
                caption: 'Country',
                lookup: {
                    dataSource: countries,
                    displayExpr: 'Name',
                    valueExpr: 'Id',
                },
                validationRules: [{ type: 'required' }],
            },
            {
                dataField: 'StartDate',
                dataType: 'date',
                format: 'dd-MMM-yyyy',
                validationRules: [{ type: 'required' }],
            }, {
                dataField: 'EndDate',
                dataType: 'date',
                format: 'dd-MMM-yyyy',
            }, {
                dataField: 'ProjectId',
                caption: 'Project',
                lookup: {
                    dataSource: projects,
                    displayExpr: 'Name',
                    valueExpr: 'Id',
                },
                validationRules: [{ type: 'required' }],
            },
            {
                type: 'buttons',
                buttons: ['edit', 'delete'],
            },
            ],
            onFocusedRowChanging(e) {
                const rowsCount = e.component.getVisibleRows().length;
                const pageCount = e.component.pageCount();
                const pageIndex = e.component.pageIndex();
                const key = e.event && e.event.key;

                if (key && e.prevRowIndex === e.newRowIndex) {
                    if (e.newRowIndex === rowsCount - 1 && pageIndex < pageCount - 1) {
                        e.component.pageIndex(pageIndex + 1).done(() => {
                            e.component.option('focusedRowIndex', 0);
                        });
                    } else if (e.newRowIndex === 0 && pageIndex > 0) {
                        e.component.pageIndex(pageIndex - 1).done(() => {
                            e.component.option('focusedRowIndex', rowsCount - 1);
                        });
                    }
                }
            },
            onFocusedRowChanged(e) {
                var deferredDetails = $.Deferred();
                console.log(1);
                WebApplication3.WebService.ProjectDetails(e.row.data.ProjectId, result => deferredDetails.resolve(JSON.parse(result)));
                $.when(deferredDetails).done(function (result) {
                    console.log(2);
                    console.log(result);
                    console.log(result[0].Type);
                    console.log(result[0].Description);
                    $('#taskSubject').html("Project type: " + (result[0].Type == 0 ? "Research" :
                        result[0].Type == 1 ? "New Product Development" : "Computer Software Development"));
                    $('#taskDetails').html(result[0].Description);
                })
            },
            onSaving(e) {
                if (e.changes.length != 0) {
                    var recordData;
                    var methodRes;
                    if (e.changes[0].type === "remove")
                        WebApplication3.WebService.DeleteRecord(e.changes[0].key, "Admin", function () { methodRes = true }, function () { methodRes = false });
                    else {
                        if (e.changes[0].type === "update") {
                            recordData = {
                                CountryId: e.changes[0].data.CountryId === undefined ? records.find(element => element.Id == e.changes[0].key).CountryId : e.changes[0].data.CountryId,
                                Id: e.changes[0].key,
                                Name: e.changes[0].data.Name === undefined ? records.find(element => element.Id == e.changes[0].key).Name : e.changes[0].data.Name,
                                ProjectId: e.changes[0].data.ProjectId === undefined ? records.find(element => element.Id == e.changes[0].key).ProjectId : e.changes[0].data.ProjectId,
                                StartDate: e.changes[0].data.StartDate === undefined ? records.find(element => element.Id == e.changes[0].key).StartDate : e.changes[0].data.StartDate,
                                EndDate: e.changes[0].data.EndDate === undefined ? records.find(element => element.Id == e.changes[0].key).EndDate : e.changes[0].data.EndDate,
                                Description: e.changes[0].data.Description === undefined ? records.find(element => element.Id == e.changes[0].key).Description : e.changes[0].data.Description
                            }
                        }
                        else
                            recordData = {
                                CountryId: e.changes[0].data.CountryId,
                                Id: -1,
                                Name: e.changes[0].data.Name,
                                ProjectId: e.changes[0].data.ProjectId,
                                StartDate: e.changes[0].data.StartDate,
                                EndDate: e.changes[0].data.EndDate === undefined ? "" : e.changes[0].data.EndDate,
                                Description: e.changes[0].data.Description === undefined ? "" : e.changes[0].data.Description
                            }
                        WebApplication3.WebService.UpdateTable(JSON.stringify(recordData), "Admin", result => function (result) { result == "-1" ? methodRes = true : methodRes = false }, function () { methodRes = false })

                    }
                    if (methodRes === true) {
                        alert("Something went wrong, the data was not saved");
                    }
                }
            },
            expandedRowKeys: [1],

        }).dxDataGrid('instance');
})
    /*
    $('#taskId').dxNumberBox({
        min: 1,
        max: 183,
        step: 0,
        onValueChanged(e) {
            if (e.event && e.value > 0) {
                dataGrid.option('focusedRowKey', e.value);
            }
        },
    });
    
    $('#autoNavigateCheckboxId').dxCheckBox({
        text: 'Auto Navigate To Focused Row',
        value: true,
        onValueChanged(e) {
            dataGrid.option('autoNavigateToFocusedRow', e.value);
        },
    });*/
});

function getTaskDataItem(row) {
    const rowData = row && row.data;
    const taskItem = {
        ProjectType: '',
        Description: '',
    };
    if (rowData) {
        WebApplication3.WebService.ProjectDetails(rowData.ProjectId, function (details) {
            console.log(details);
            taskItem.ProjectType = details.Type;
            taskItem.Description = details.Description;
        })
    }

    return taskItem;
}