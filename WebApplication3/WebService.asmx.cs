﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web.Script.Services;
using System.Web.Services;
using WebApplication3.Models;

namespace WebApplication3
{
    [System.Web.Script.Services.ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WebService : System.Web.Services.WebService
    {
        private SqlConnection connection = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Records;Integrated Security=True");

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetData(string table)
        {
            string select;
            SqlDataReader reader;
            SqlCommand cmd;
            switch (table)
            {
                case ("Records"):
                    select = "exec MY_EntityRecordsList_Master";
                    cmd = new SqlCommand(select, connection);
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    return JsonConvert.SerializeObject(GetList<RecordViewModel>(reader));

                case ("Projects"):
                    select = "exec MY_ProjectsList_Filter";
                    cmd = new SqlCommand(select, connection);
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    return JsonConvert.SerializeObject(GetList<ProjectViewModel>(reader));

                case ("Countries"):
                    select = "exec MY_CountriesList_Filter";
                    cmd = new SqlCommand(select, connection);
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    return JsonConvert.SerializeObject(GetList<CountryViewModel>(reader));
                case ("Regions"):
                    select = "exec MY_RegionsList_Filter";
                    cmd = new SqlCommand(select, connection);
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    return JsonConvert.SerializeObject(GetList<RegionViewModel>(reader));
                case ("Cities"):
                    return JsonConvert.SerializeObject(GetCityListItemViewModels());
                case ("GroupedCountries"):
                    select = "select " +
                                    "COUNTRY_CODE, " +
                                    "REGION_ID, " +
                                    "RECORD_ID, " +
                                    "COUNTRY_NAME " +
                             "from COUNTRIES";
                    cmd = new SqlCommand(select, connection);
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    return JsonConvert.SerializeObject(GetList<CountryWithRegionViewModel>(reader));
                default:
                    return "";
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ProjectDetails(int id)
        {
            var cmd = new SqlCommand("exec MY_Project_Details " + id, connection);
            connection.Open();
            var reader = cmd.ExecuteReader();
            return JsonConvert.SerializeObject(GetList<ProjectDetails>(reader));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string CountryDetails(int id)
        {
            var cmd = new SqlCommand("exec MY_Country_Details " + id, connection);
            connection.Open();
            var reader = cmd.ExecuteReader();
            return JsonConvert.SerializeObject(GetList<RegionViewModel>(reader));
        }

        private List<T> GetList<T>(IDataReader reader)
        {
            var list = new List<T>();
            while (reader.Read())
            {
                var type = typeof(T);
                T obj = (T)Activator.CreateInstance(type);
                var columnIndex = 0;
                foreach (var prop in type.GetProperties())
                {
                    var propType = prop.Name != "EndDate" ? prop.PropertyType : typeof(DateTime);
                    try
                    {
                        var check = reader[columnIndex].ToString();
                        prop.SetValue(obj, Convert.ChangeType(reader[columnIndex].ToString(), propType));
                    }
                    catch { }
                    columnIndex++;
                }
                list.Add(obj);
            }
            return list;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string UpdateTable(string recordInfo, string userName)
        {
            RecordViewModel recordData = JsonConvert.DeserializeObject<RecordViewModel>(recordInfo);
            var cmd = new SqlCommand("exec MY_EntityRecords_Update " +
                                                                       "@RECORD_ID=@ID" +
                                                                      ",@RECORD_NAME=@NAME" +
                                                                      ",@COUNTRY_ID=@CID" +
                                                                      ",@PROJECT_ID=@PID" +
                                                                      ",@START_DATE=@SD" +
                                                                      (recordData.EndDate != null ? ",@END_DATE=@ED" : "") +
                                                                      ",@RECORD_DESCRIPTION=@RD" +
                                                                      ",@USER_ID=@UN", connection);
            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = recordData.Id;
            cmd.Parameters.Add("@NAME", SqlDbType.Text);
            cmd.Parameters["@NAME"].Value = recordData.Name;
            cmd.Parameters.Add("@CID", SqlDbType.Int);
            cmd.Parameters["@CID"].Value = recordData.CountryId;
            cmd.Parameters.Add("@PID", SqlDbType.Int);
            cmd.Parameters["@PID"].Value = recordData.ProjectId;
            cmd.Parameters.Add("@SD", SqlDbType.DateTime);
            cmd.Parameters["@SD"].Value = recordData.StartDate;
            if (recordData.EndDate != null)
            {
                cmd.Parameters.Add("@ED", SqlDbType.DateTime);
                cmd.Parameters["@ED"].Value = recordData.EndDate;
            }
            cmd.Parameters.Add("@RD", SqlDbType.Text);
            cmd.Parameters["@RD"].Value = recordData.Description;
            cmd.Parameters.Add("@UN", SqlDbType.Text);
            cmd.Parameters["@UN"].Value = userName;
            connection.Open();

            return JsonConvert.SerializeObject(cmd.ExecuteNonQuery());
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string DeleteRecord(int id, string userName)
        {
            var cmd = new SqlCommand("exec MY_EntityRecords_Delete @RECORD_ID=@ID, @USER_NAME=@UN", connection);
            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = id;
            cmd.Parameters.Add("@UN", SqlDbType.Text);
            cmd.Parameters["@UN"].Value = userName;
            connection.Open();

            return JsonConvert.SerializeObject(cmd.ExecuteNonQuery());
        }

        private IEnumerable<CityListItemViewModel> GetCityListItemViewModels()
        {
            List<CityListItemViewModel> listRegions = new List<CityListItemViewModel>();
            List<CityListItemViewModel> listCountries = new List<CityListItemViewModel>();
            List<CityListItemViewModel> listCities = new List<CityListItemViewModel>();
            var cmdRegions = new SqlCommand("select " +
                                                    "0, " +
                                                    "0, " +
                                                    "RE.RECORD_ID, " +
                                                    "RE.REGION_NAME " +
                                            "from REGIONS RE"
                                            , connection);
            var cmdCountries = new SqlCommand("select distinct " +
                                                        "CO.COUNTRY_CODE, " +
                                                        "CO.REGION_ID, " +
                                                        "CO.RECORD_ID, " +
                                                        "CO.COUNTRY_NAME " +
                                              "from COUNTRIES CO " +
                                              "join CITIES CI on CI.COUNTRY_ID = CO.RECORD_ID"
                                            , connection);
            var cmdCities = new SqlCommand("select " +
                                                "0, " +
                                                "CY.COUNTRY_ID," +
                                                "CY.RECORD_ID, " +
                                                "CY.CITY_NAME " +
                                                "FROM CITIES CY"
                                           , connection); ;
            connection.Open();

            var readerRegions = cmdRegions.ExecuteReader();
            listRegions = GetList<CityListItemViewModel>(readerRegions);
            readerRegions.Close();
            var readerCountries = cmdCountries.ExecuteReader();
            listCountries = GetList<CityListItemViewModel>(readerCountries);
            readerCountries.Close();
            var readerCities = cmdCities.ExecuteReader();
            listCities = GetList<CityListItemViewModel>(readerCities);

            listCountries.ForEach(country => country.ParentId = listRegions.FindIndex(region => region.Id == country.ParentId) + 1);
            listCities.ForEach(city => city.ParentId = listCountries.FindIndex(country => country.Id == city.ParentId) + listRegions.Count + 1);

            return listRegions.Union(listCountries.Union(listCities)).AsEnumerable();

        }
    }
}