﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication3
{
    public class SampleContext : DbContext
    {
        public SampleContext()
        { }

        public DbSet<Record> Records { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Project> Projects { get; set; }
        
    }
}