﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DECitiesTree.aspx.cs" Inherits="WebApplication3.DECitiesTree" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    Cities
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>window.jQuery || document.write(decodeURIComponent('%3Cscript src="js/jquery.min.js"%3E%3C/script%3E'))</script>
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/22.1.6/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/22.1.6/css/dx.light.css" />
    <script src="https://cdn3.devexpress.com/jslib/22.1.6/js/dx.all.js"></script>
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script src="DETreeListScript.js"></script>
    <style>
        #tasks {
  max-height: 540px;
}

#tasks .dx-treelist-rowsview td {
  vertical-align: middle;
}

.img,
.name {
  display: inline-block;
  vertical-align: middle;
}

.img {
  height: 25px;
  width: 25px;
  margin-right: 10px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 25px;
  border: 2px solid lightgray;
  background-color: #fff;
}
    </style>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="~/WebService.asmx" />
            </Services>
        </asp:ScriptManager>
        <div class="demo-container">
            <div id="tasks"></div>
        </div>
    </form>
</asp:Content>
