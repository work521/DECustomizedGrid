﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CreateNew.aspx.cs" Inherits="WebApplication3.CreateNew" Theme="Skin1"%>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    Create New Record
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <link rel="stylesheet" href="App_Themes/DetailsStyles.css">
    <div class="RecLabelDiv">

        <asp:Label ID="LabelID" runat="server" BackColor="#6D9CB5" CssClass="RecordLabel" ForeColor="White" text="Record ID: "></asp:Label>
    </div>
    <div class="GeneralDataDiv">
        General data
    </div>
    <form id="form1" runat="server">
        <asp:Table ID="Table1" runat="server" CssClass="DetailsTable">
            <asp:TableHeaderRow CssClass="GeneralDataDiv"></asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Name:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:TextBox ID="tb_Name" runat="server" CssClass="LongInput TextBox InputControl"></asp:TextBox>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Project:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:DropDownList ID="ddl_Project" runat="server" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="ProjectId" CssClass="LongInput InputControl">
                    </asp:DropDownList>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Country:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:DropDownList ID="ddl_Country" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="CountryId" CssClass="LongInput InputControl">
                    </asp:DropDownList>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Start date:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:TextBox ID="tb_StartDate" runat="server" CssClass="ShortInput TextBox InputControl"></asp:TextBox>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    End date:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:TextBox ID="tb_EndDate" runat="server" CssClass="ShortInput TextBox InputControl"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="SELECT * FROM [Countries]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="SELECT * FROM [Projects]"></asp:SqlDataSource>
        <div>
            <asp:Label ID="ValidationLabel" runat="server" Visible="false" CssClass="ValidationLabel"></asp:Label>
        </div>
        <div class="ButtonsDiv">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
        </div>
    </form>
</asp:Content>
