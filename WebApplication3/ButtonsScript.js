﻿function BindData() {
    WebApplication3.WebService.GetData("Records", OnRequestComplete);
}
function OnRequestComplete(result) {
    var records = JSON.parse(result);
    console.log(records);
    document.getElementById("table").style = "visibility: visible";
    const tBody = document.getElementById("tBody");
    $("#tBody tr").remove();
    for (var n = 0; n < records.length; n++) {
        var tr = document.createElement("tr");
        var div = document.createElement("div");
        div.className = "recordCircle";
        div.style = "background:var(--leftP" + records[n].ProjectId + "),var(--center),var(--rightC" + records[n].CountryId + ")";
        tr.className = RowClass(records[n]);
        tr.insertCell(0);
        tr.cells[0].appendChild(div);
        var divName = document.createElement("div");
        divName.style = "margin-top:1.5px";
        divName.textContent = records[n].Name;
        tr.cells[0].appendChild(divName);
        tr.insertCell(1).textContent = records[n].ProjectId;
        tr.insertCell(2).textContent = new Date(Date.parse(records[n].StartDate)).format("dd-MMM-yyyy");
        if (records[n].EndDate != null)
            tr.insertCell(3).textContent = new Date(Date.parse(records[n].EndDate)).format("dd-MMM-yyyy");
        else
            tr.insertCell(3);
        tr.insertCell(4).textContent = records[n].CountryId;
        tBody.appendChild(tr);
    }
}
function RowClass(record) {
    if (new Date(Date.parse(record.StartDate)) > new Date())
        return "NotStarted rowhighlight";
    else {
        if (record.EndDate != null)
            return new Date(Date.parse(record.EndDate)) < new Date() ? "Ended rowhighlight" : "NotEnded rowhighlight";
        else
            return "NotEnded rowhighlight";
    }
}

function BindAllData() {
    var deferredCountries = $.Deferred();
    var deferredProjects = $.Deferred();

    $.when(deferredCountries, deferredProjects).done(function (countries, projects) {
        console.log(countries);
        console.log(projects);
        document.getElementById("tableCountries").style = "visibility: visible";
        document.getElementById("tableProjects").style = "visibility: visible";
        const tBodyCountries = document.getElementById("tBodyCountries");
        $("#tBodyCountries tr").remove();
        for (var n = 0; n < countries.length; n++) {
            var tr = document.createElement("tr");

            tr.className = RowClass(countries[n]);
            tr.insertCell(0).textContent = countries[n].Id;
            tr.insertCell(1).textContent = countries[n].Name.trim();
            tBodyCountries.appendChild(tr);
        }
        const tBodyProjects = document.getElementById("tBodyProjects");
        $("#tBodyProjects tr").remove();
        for (var n = 0; n < projects.length; n++) {
            var tr = document.createElement("tr");
            tr.className = RowClass(projects[n]);
            tr.insertCell(0).textContent = projects[n].Id;
            tr.insertCell(1).textContent = projects[n].Name.trim();
            tBodyProjects.appendChild(tr);
        }
    });
    WebApplication3.WebService.GetData("Countries", result => deferredCountries.resolve(JSON.parse(result)));
    WebApplication3.WebService.GetData("Projects", result => deferredProjects.resolve(JSON.parse(result)));
}