﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace WebApplication3
{
    public static class ValidationException
    {
        public static void ValidateControl(WebControl control)
        {
            if (control.GetType() == typeof(TextBox))
                ValidateTextBox((TextBox)control);
            if (control.GetType() == typeof(DropDownList))
                ValidateDDL((DropDownList)control);
        }
        public static void ValidateTextBox(TextBox control)
        {
            if (control.Text == "")
            {
                throw new Exception(control.ID.Substring(3) + ": may not be empty (required field)");
            }
        }
        public static void ValidateDDL(DropDownList control)
        {
            if (control.SelectedValue == "")
            {
                throw new Exception(control.ID.Substring(4) + ": may not be empty (required field)");
            }
        }
        public static void ValidateDate(TextBox control)
        {
            if (!DateTime.TryParse(control.Text, out _))
                throw new Exception(control.ID.Substring(3) + ": wrong date format");
        }
    }
}