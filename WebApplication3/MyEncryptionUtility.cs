﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WebApplication3
{
    public static class MyEncryptionUtility
    {
        public static class SymmetricEncryptionUtility
        {
            public static string AlgorithmName ="DES";
            public static void GenerateKey()
            {
                // Создать алгоритм
                SymmetricAlgorithm Algorithm = SymmetricAlgorithm.Create(AlgorithmName);
                Algorithm.GenerateKey();

                // Получить ключ
                byte[] Key = Algorithm.Key;
                // Использовать DPAPI для шифрования ключа
                Key = ProtectedData.Protect(Key, null, DataProtectionScope.CurrentUser);

                HttpContext.Current.Session["Key"] = Key;
            }

            public static void ReadKey(SymmetricAlgorithm algorithm)
            {
                byte[] Key = (byte[])HttpContext.Current.Session["Key"];
                algorithm.Key = ProtectedData.Unprotect(Key, null, DataProtectionScope.CurrentUser);
            }

            public static byte[] EncryptData(string data)
            {
                // Преобразовать строку data в байтовый массив
                byte[] ClearData = Encoding.UTF8.GetBytes(data);

                // Создать алгоритм шифрования
                SymmetricAlgorithm Algorithm = SymmetricAlgorithm.Create(AlgorithmName);
                ReadKey(Algorithm);

                // Зашифровать информацию
                MemoryStream Target = new MemoryStream();

                // Сгенерировать случайный вектор инициализации (IV)
                // для использования с алгоритмом
                Algorithm.GenerateIV();
                Target.Write(Algorithm.IV, 0, Algorithm.IV.Length);

                // Зашифровать реальные данные
                CryptoStream cs = new CryptoStream(Target, Algorithm.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(ClearData, 0, ClearData.Length);
                cs.FlushFinalBlock();

                // Вернуть зашифрованный поток данных в виде байтового массива
                return Target.ToArray();
            }

            public static string DecryptData(byte[] data)
            {
                // Создать алгоритм
                SymmetricAlgorithm Algorithm = SymmetricAlgorithm.Create(AlgorithmName);
                ReadKey(Algorithm);

                // Расшифровать информацию
                MemoryStream Target = new MemoryStream();

                // Прочитать вектор инициализации (IV)
                // и инициализировать им алгоритм
                int ReadPos = 0;
                byte[] IV = new byte[Algorithm.IV.Length];
                Array.Copy(data, IV, IV.Length);
                Algorithm.IV = IV;
                ReadPos += Algorithm.IV.Length;

                CryptoStream cs = new CryptoStream(Target, Algorithm.CreateDecryptor(),
                    CryptoStreamMode.Write);
                cs.Write(data, ReadPos, data.Length - ReadPos);
                cs.FlushFinalBlock();

                // Получить байты из потока в памяти и преобразовать их в текст
                return Encoding.UTF8.GetString(Target.ToArray());
            }
        }
    }
}