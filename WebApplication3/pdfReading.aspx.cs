﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using WebApplication3.Models;

namespace WebApplication3
{
    public partial class WebForm3 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Click(object sender, EventArgs e)
        {
            DirectoryInfo d = new DirectoryInfo(tb.Text);

            FileInfo[] Files = d.GetFiles("*.pdf");
            foreach (var file in Files)
            {
                PdfReader pdfReader = new PdfReader(file.FullName);
                PdfDocument pdfDoc = new PdfDocument(pdfReader);
                string text = "";
                for (int page = 1; page <= pdfDoc.GetNumberOfPages(); page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                    text += PdfTextExtractor.GetTextFromPage(pdfDoc.GetPage(page), strategy);

                }
                using (var connection = new SqlConnection("Data Source=NBSPB625\\MSSQLSERVER1;Initial Catalog=PDFs;Integrated Security=True"))
                {
                    var cmd = new SqlCommand("insert into PDFs (FILE_TEXT, FILE_PATH) " +
                        "VALUES (@TEXT, @PATH)", connection);
                    connection.Open();
                    cmd.Parameters.Add("@TEXT", SqlDbType.Text);
                    cmd.Parameters["@TEXT"].Value = text.Trim();
                    cmd.Parameters.Add("@PATH", SqlDbType.Text);
                    cmd.Parameters["@PATH"].Value = file.FullName;
                    cmd.ExecuteNonQuery();

                }
                pdfDoc.Close();
                pdfReader.Close();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text.Trim() == "")
            {
                var cmdText = "select [RECORD_ID] , [FILE_PATH] from PDFs.DBO.PDFs where contains([file_text],@TEXT)";
                var resultList = new List<FileData>();
                using (var connection = new SqlConnection("Data Source=NBSPB625\\MSSQLSERVER1;Initial Catalog=PDFs;Integrated Security=True"))
                {
                    connection.Open();
                    var cmd = new SqlCommand(cmdText, connection);
                    cmd.Parameters.Add("@TEXT", SqlDbType.NVarChar);
                    cmd.Parameters["@TEXT"].Value = "\"" + TextBox1.Text.Trim() + "\"";
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        resultList.Add(new FileData(Convert.ToInt32(reader[0].ToString()), reader[1].ToString()));
                    }
                }
                GridView1.DataSource = resultList;
                GridView1.DataBind();
            }
        }
    }
}