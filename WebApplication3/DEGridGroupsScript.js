﻿$(() => {
    var deferredCountries = $.Deferred();
    var deferredRegions = $.Deferred();

    WebApplication3.WebService.GetData("GroupedCountries", result => deferredCountries.resolve(JSON.parse(result)));
    WebApplication3.WebService.GetData("Regions", result => deferredRegions.resolve(JSON.parse(result)));
    $.when(deferredCountries, deferredRegions).done(function (countries, regions) {
        $('#gridContainer').dxDataGrid({
            dataSource: countries,
            keyExpr: 'Id',
            allowColumnReordering: true,
            showBorders: true,
            grouping: {
                autoExpandAll: true,
            },
            focusedRowEnabled: true,
            paging: {
                pageSize: 10,
            },
            groupPanel: {
                visible: true,
            },
            columns: [
                {
                    dataField: 'Name',
                    cellTemplate(container, options) {
                        container
                            .append($('<div>', { class: 'img', style: `background-image:url(https://countryflagsapi.com/png/${options.data.FlagUNCode});` }))
                            .append('\n')
                            .append($('<span>', { class: 'name', text: options.data.Name }));
                    },
                },
                {
                    dataField: 'RegionId',
                    caption: 'Region name',
                    lookup: {
                        dataSource: regions,
                        displayExpr: 'Name',
                        valueExpr: 'Id',
                    },
                    groupIndex: 0,
                },
            ],
            onFocusedRowChanging(e) {
                const rowsCount = e.component.getVisibleRows().length;
                const pageCount = e.component.pageCount();
                const pageIndex = e.component.pageIndex();
                const key = e.event && e.event.key;

                if (key && e.prevRowIndex === e.newRowIndex) {
                    if (e.newRowIndex === rowsCount - 1 && pageIndex < pageCount - 1) {
                        e.component.pageIndex(pageIndex + 1).done(() => {
                            e.component.option('focusedRowIndex', 0);
                        });
                    } else if (e.newRowIndex === 0 && pageIndex > 0) {
                        e.component.pageIndex(pageIndex - 1).done(() => {
                            e.component.option('focusedRowIndex', rowsCount - 1);
                        });
                    }
                }
            },
            onFocusedRowChanged(e) {
                var deferredDetails = $.Deferred();
                console.log(1);
                WebApplication3.WebService.CountryDetails(e.row.data.Id, result => deferredDetails.resolve(JSON.parse(result)));
                $.when(deferredDetails).done(function (result) {
                    $('#taskSubject').html(e.row.data.Name);
                    var records = "";
                    result.forEach(item => records += item.Name + ', ');
                    
                    console.log(result);
                    console.log(records);
                    $('#taskDetails').html("Records: " + records.substring(0, records.length - 2));
                })
            },
        }).dxDataGrid('instance');
    });
});
