﻿$(() => {
    var deferredCities = $.Deferred();

    WebApplication3.WebService.GetData("Cities", result => deferredCities.resolve(JSON.parse(result)));
    $.when(deferredCities).done(function (cities) {
        console.log(cities);
        var i = 1;
        cities.forEach(element => element.Ind = i++);
        $('#tasks').dxTreeList({
            dataSource: cities,
            keyExpr: 'Ind',
            parentIdExpr: 'ParentId',
            columnAutoWidth: true,
            wordWrapEnabled: true,
            showBorders: true,
            columns: [{
                dataField: 'Name',
                cellTemplate(container, options) {
                    const CountryFlagUNCode = options.data.CountryFlagUNCode;
                    if (CountryFlagUNCode !="0") {
                        container
                            .append($('<div>', { class: 'img', style: `background-image:url(https://countryflagsapi.com/png/${CountryFlagUNCode});` }))
                            .append('\n')
                            .append($('<span>', { class: 'name', text: options.data.Name }));
                    }
                    else {
                        container
                    .append('\n')
                    .append($('<span>', { class: 'name', text: options.data.Name }));
                    }
                },
            }],
        });
    });
});
