﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="pdfReading.aspx.cs" Inherits="WebApplication3.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    PDF reading
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form id="form1" runat="server">
        <div style="margin-bottom:50px">
            <asp:TextBox runat="server" ID="tb"></asp:TextBox>
        <asp:Button runat="server" Text="Upload" ID="btn" OnClick="btn_Click" />
        </div>
        <div style="margin-bottom:50px">
        <asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" />
        </div>
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PDFsConnectionString2 %>" SelectCommand="SELECT * FROM [PDFs]"></asp:SqlDataSource>
    </form>
</asp:Content>
