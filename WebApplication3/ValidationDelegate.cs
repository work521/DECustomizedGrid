﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3
{
    public delegate void UnvalidParameter(string parameterName);
    public static class ValidationDelegate
    {
        public static UnvalidParameter invalidParameterDelegate;
        public static bool IsValid;
        public static void ValidateName(string Name)
        {
            if (Name == "")
                invalidParameterDelegate("Name: may not be empty (required field)");
            /*string commandTextName = "Select COUNT(*) from Records where Name = @NAME and Id!=@ID";
            SqlCommand commandName = new SqlCommand(commandTextName, connection);
            commandName.Parameters.Add("@NAME", SqlDbType.NChar);
            commandName.Parameters["@NAME"].Value = tbName.Text;
            string a = Request.QueryString["id"].Replace(" ", "+");
            byte[] data = Convert.FromBase64String(a);
            commandName.Parameters.Add("@ID", SqlDbType.Int);
            commandName.Parameters["@ID"].Value = SymmetricEncryptionUtility.DecryptData(data);
            if ((int)commandName.ExecuteScalar() != 0)
            {
                unvalidParameter("Name: Record with this name already exist's");
            }*/
        }

        public static void ValidateCountry(string CountryId)
        {
            if (CountryId == "")
                invalidParameterDelegate("Country: may not be empty (required field)");
        }

        public static void ValidateProject(string ProjectId)
        {
            if (ProjectId == "")
                invalidParameterDelegate("Project: may not be empty (required field)");
        }

        public static void ValidateStartDate(string StartDate)
        {
            if (StartDate == "")
                invalidParameterDelegate("Start date: may not be empty (required field)");
            if (!DateTime.TryParse(StartDate, out _))
                invalidParameterDelegate("Start date: wrong date format");
        }
        public static void ValidateEndDate(string EndDate)
        {
            if (EndDate == "")
                return;
            if (!DateTime.TryParse(EndDate, out _))
                invalidParameterDelegate("End date: wrong date format");
        }
    }
}