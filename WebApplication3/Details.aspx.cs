﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using System.Web.Caching;
using static WebApplication3.MyEncryptionUtility;
using static WebApplication3.ValidationException;

namespace WebApplication3
{
    public partial class Details : System.Web.UI.Page
    {
        private SqlConnection connection = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=EntityRecords;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Request.QueryString.Count != 0)
            {
                Fill_Data();
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            string commandText = "Delete from EntityRecords where Id = @ID";
            SqlCommand command = new SqlCommand(commandText, connection);
            string a = Request.QueryString["id"].Replace(" ", "+");
            byte[] data = Convert.FromBase64String(a);
            command.Parameters.Add("@ID", SqlDbType.Int);
            command.Parameters["@ID"].Value = SymmetricEncryptionUtility.DecryptData(data);
            connection.Open();
            command.ExecuteNonQuery();
            Response.Redirect("~/Default.aspx");
            ValidationLabel.Text = "";
        }
        protected void ButtonClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            ValidationLabel.Visible = false;
            ChangeEnable();
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (Valid())
                Save();
        }
        protected bool Valid()
        {
            try
            {
                ValidateControl(tb_Name);
                ValidateControl(ddl_Project);
                ValidateControl(ddl_Country);
                ValidateControl(tb_StartDate);
                ValidateDate(tb_StartDate);
                if (tb_EndDate.Text != "")
                    ValidateDate(tb_EndDate);
            }
            catch (Exception ex)
            {
                ValidationLabel.Visible = true;
                ValidationLabel.Text = ex.Message;
                ValidationLabel.BackColor = System.Drawing.Color.FromArgb(166,44,41);
                return false;
            }
            return true;
        }
        protected void Save()
        {
            string commandText = "update EntityRecords set Name=@NAME, CountryId=@COUNTRYID, ProjectId=@PROJECTID, StartDate=@STARTDATE, EndDate=@ENDDATE where Id = @ID";
            SqlCommand command = new SqlCommand(commandText, connection);
            string a = Request.QueryString["id"].Replace(" ", "+");
            byte[] data = Convert.FromBase64String(a);
            command.Parameters.Add("@NAME", SqlDbType.Text);
            command.Parameters["@NAME"].Value = tb_Name.Text;
            command.Parameters.Add("@COUNTRYID", SqlDbType.Int);
            command.Parameters["@COUNTRYID"].Value = ddl_Country.SelectedValue;
            command.Parameters.Add("@PROJECTID", SqlDbType.Int);
            command.Parameters["@PROJECTID"].Value = ddl_Project.SelectedValue;
            command.Parameters.Add("@STARTDATE", SqlDbType.Date);
            command.Parameters["@STARTDATE"].Value = tb_StartDate.Text;
            command.Parameters.Add("@ENDDATE", SqlDbType.Date);
            if (DateTime.TryParse(tb_EndDate.Text, out _))
            {
                command.Parameters["@ENDDATE"].Value=DateTime.Parse(tb_EndDate.Text);
            }
            else
            {
                command.Parameters["@ENDDATE"].Value = DBNull.Value;
            }
            command.Parameters.Add("@ID", SqlDbType.Int);
            command.Parameters["@ID"].Value = SymmetricEncryptionUtility.DecryptData(data);
            connection.Open();
            command.ExecuteNonQuery();
            ValidationLabel.Visible = true;
            ValidationLabel.Text = "Record was saved to database";
            ValidationLabel.BackColor = System.Drawing.Color.FromArgb(81, 136, 77); ;
            ChangeEnable();
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {

            ChangeEnable();
            Fill_Data();
            ValidationLabel.Visible = false;
        }



        private void ChangeEnable()
        {
            tb_Name.Enabled = !tb_Name.Enabled;
            ddl_Country.Enabled = !ddl_Country.Enabled;
            ddl_Project.Enabled = !ddl_Project.Enabled;
            tb_StartDate.Enabled = !tb_StartDate.Enabled;
            tb_EndDate.Enabled = !tb_EndDate.Enabled;
            ButtonSave.Visible = !ButtonSave.Visible;
            ButtonEdit.Visible = !ButtonEdit.Visible;
            ButtonDelete.Visible = !ButtonDelete.Visible;
            ButtonCancel.Visible = !ButtonCancel.Visible;
            ButtonClose.Visible = !ButtonClose.Visible;
        }
        private void Fill_Data()
        {
            string a = Request.QueryString["id"].Replace(" ", "+");
            byte[] data = Convert.FromBase64String(a);
            string commandText = "Select * from EntityRecords where Id = @ID";
            SqlCommand command = new SqlCommand(commandText, connection);
            command.Parameters.Add("@ID", SqlDbType.Int);
            command.Parameters["@ID"].Value = SymmetricEncryptionUtility.DecryptData(data);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            LabelID.Text = "Record ID: " + SymmetricEncryptionUtility.DecryptData(data);
            tb_Name.Text = dataSet.Tables[0].Rows[0].Field<string>("Name").Trim();
            ddl_Country.SelectedValue = dataSet.Tables[0].Rows[0].Field<int>("CountryId").ToString();
            ddl_Project.SelectedValue = dataSet.Tables[0].Rows[0].Field<int>("ProjectId").ToString();
            tb_StartDate.Text = dataSet.Tables[0].Rows[0].Field<DateTime>("StartDate").ToString("dd-MMM-yyyy");
            try
            {
                tb_EndDate.Text = dataSet.Tables[0].Rows[0].Field<DateTime>("EndDate").ToString("dd-MMM-yyyy");
            }
            catch
            {
                tb_EndDate.Text = "";
            }
        }
    }
}