﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DEGRidGroup.aspx.cs" Inherits="WebApplication3.DEGRidGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    Countries
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>window.jQuery || document.write(decodeURIComponent('%3Cscript src="js/jquery.min.js"%3E%3C/script%3E'))</script>
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/22.1.6/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/22.1.6/css/dx.light.css" />
    <script src="https://cdn3.devexpress.com/jslib/22.1.6/js/dx.all.js"></script>
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script src="DEGridGroupsScript.js"></script>
    <style>
        .task-info {
            font-family: Segoe UI;
            min-height: 200px;
            display: flex;
            flex-wrap: nowrap;
            border: 2px solid rgba(0, 0, 0, 0.1);
            padding: 16px;
            box-sizing: border-box;
            align-items: baseline;
            justify-content: space-between;
        }

        #taskSubject {
            line-height: 29px;
            font-size: 18px;
            font-weight: bold;
        }

        #taskDetails {
            line-height: 22px;
            font-size: 14px;
            margin-top: 0;
            margin-bottom: 0;
        }

        .options {
            padding: 20px;
            margin-top: 20px;
            background-color: rgba(191, 191, 191, 0.15);
        }

        .caption {
            font-size: 18px;
            font-weight: 500;
        }

        .option {
            margin-top: 10px;
        }

        .img,
        .name {
            display: inline-block;
            vertical-align: middle;
        }

        .img {
            height: 25px;
            width: 25px;
            margin-right: 10px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            border-radius: 25px;
            border: 2px solid lightgray;
            background-color: #fff;
        }
    </style>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="~/WebService.asmx" />
            </Services>
        </asp:ScriptManager>
        <div class="demo-container">
            <div id="gridContainer"></div>
            <div class="task-info">
                <div class="info">
                    <div id="taskSubject"></div>
                    <p id="taskDetails"></p>
                </div>
                <div class="progress">
                    <span id="taskStatus"></span>
                    <span id="taskProgress"></span>
                </div>
            </div>
        </div>

    </form>
</asp:Content>
