﻿using System;
using System.Web.UI.WebControls;
using static WebApplication3.MyEncryptionUtility;

namespace WebApplication3
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListCountry.DataBind();
                DropDownListProject.DataBind();
                DropDownListPeriod.DataBind();
                DropDownListCountry.Items.Insert(0, new ListItem("All", "0"));
                DropDownListProject.Items.Insert(0, new ListItem("All", "0"));
                DropDownListPeriod.Items.Insert(0, new ListItem("All", "0"));
                DropDownListPeriod.Items.Add(new ListItem("7 days", "7"));
                DropDownListPeriod.Items.Add(new ListItem("30 days", "30"));
                DropDownListPeriod.Items.Add(new ListItem("90 days", "90"));
                DropDownListPeriod.Items.Add(new ListItem("365 days", "365"));
                SymmetricEncryptionUtility.GenerateKey();
            }
        }

        protected string EncryptID(string Id)
        {
            byte[] data = SymmetricEncryptionUtility.EncryptData(Id);
            return Convert.ToBase64String(data);
        }

        protected void DropDownLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        private bool CheckCountry(GridViewRow gridViewRow)
        {
            if (DropDownListCountry.SelectedValue != "0")
            {
                return ((Label)gridViewRow.FindControl("CountryId")).Text == (DropDownListCountry.SelectedValue);
            }
            else
            {
                return true;
            }
        }

        private bool CheckProject(GridViewRow gridViewRow)
        {
            if (DropDownListProject.SelectedValue != "0")
            {
                return ((Label)gridViewRow.FindControl("ProjectId")).Text == (DropDownListProject.SelectedValue);
            }
            else
            {
                return true;
            }
        }

        private bool CheckDate(GridViewRow gridViewRow)
        {
            if (DropDownListPeriod.SelectedValue != "0")
            {
                try
                {
                    DateTime StartDate = DateTime.Parse(((Label)gridViewRow.FindControl("StartDate")).Text);
                    DateTime EndDate = DateTime.Parse(((Label)gridViewRow.FindControl("EndDate")).Text);
                    return (EndDate - StartDate) < new TimeSpan(Convert.ToInt32(DropDownListPeriod.SelectedValue), 0, 0, 0);
                }
                catch
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                try
                {
                    DateTime checkDate = DateTime.Parse(((Label)GridView1.Rows[i].FindControl("StartDate")).Text);
                    if (checkDate - DateTime.Now > new TimeSpan(0))
                    {
                        GridView1.Rows[i].CssClass = "NotStarted rowhighlight";
                    }
                    else
                    {
                        if (((Label)GridView1.Rows[i].FindControl("EndDate")).Text != "")
                        {
                            checkDate = DateTime.Parse(((Label)GridView1.Rows[i].FindControl("EndDate")).Text);
                            GridView1.Rows[i].CssClass = (checkDate - DateTime.Now > new TimeSpan(0)) ? "NotEnded rowhighlight" : "Ended rowhighlight";
                        }
                        else
                        {
                            GridView1.Rows[i].CssClass = "NotEnded rowhighlight";
                        }
                    }
                }
                catch
                {
                }
            }
            foreach (GridViewRow gridViewRow in GridView1.Rows)
            {
                gridViewRow.Visible = CheckCountry(gridViewRow) && CheckProject(gridViewRow) && CheckDate(gridViewRow);
            }
        }
    }
}