﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DEGrid.aspx.cs" Inherits="WebApplication3.WebForm2" Theme="Skin1" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    Records
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>window.jQuery || document.write(decodeURIComponent('%3Cscript src="js/jquery.min.js"%3E%3C/script%3E'))</script>
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/22.1.5/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/22.1.5/css/dx.light.css" />
    <link rel="stylesheet" href="App_Themes/GridViewStyles.css">
    <script src="https://cdn3.devexpress.com/jslib/22.1.5/js/dx.all.js"></script>
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script src="DEScript.js"></script>
    <style>
        .task-info {
            font-family: Segoe UI;
            min-height: 200px;
            display: flex;
            flex-wrap: nowrap;
            border: 2px solid rgba(0, 0, 0, 0.1);
            padding: 16px;
            box-sizing: border-box;
            align-items: baseline;
            justify-content: space-between;
        }

        #taskSubject {
            line-height: 29px;
            font-size: 18px;
            font-weight: bold;
        }

        #taskDetails {
            line-height: 22px;
            font-size: 14px;
            margin-top: 0;
            margin-bottom: 0;
        }

        .progress {
            display: flex;
            flex-direction: column;
            white-space: pre;
            min-width: 105px;
        }

        .info {
            margin-right: 40px;
        }

        #taskProgress {
            line-height: 42px;
            font-size: 40px;
            font-weight: bold;
        }

        .options {
            margin-top: 20px;
            padding: 20px;
            background-color: rgba(191, 191, 191, 0.15);
            position: relative;
        }

        .caption {
            font-size: 18px;
            font-weight: 500;
        }

        .option {
            margin-top: 10px;
            margin-right: 40px;
            display: inline-block;
        }

            .option:last-child {
                margin-right: 0;
            }

            .option > .dx-numberbox {
                width: 200px;
                display: inline-block;
                vertical-align: middle;
            }

            .option > span {
                margin-right: 10px;
            }

        .options-container {
            display: flex;
            align-items: center;
        }
    </style>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="~/WebService.asmx" />
            </Services>
        </asp:ScriptManager>
        <div class="demo-container">
            <div id="RecordsGrid"></div>
            <div class="task-info" id="taskInfo" style="min-height: 0px">
                <div class="info">
                    <div id="taskSubject"></div>
                    <p id="taskDetails"></p>
                </div>
                <div class="progress">
                    <span id="taskStatus"></span>
                    <span id="taskProgress"></span>
                </div>
            </div>
        </div>

    </form>
</asp:Content>
