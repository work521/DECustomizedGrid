﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="WebApplication3.Details" Theme="Skin1"%>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    Details
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <link rel="stylesheet" href="App_Themes/DetailsStyles.css">
    <div class="RecLabelDiv">
        <asp:Label ID="LabelID" runat="server" BackColor="#6D9CB5" CssClass="RecordLabel" ForeColor="White"></asp:Label>
    </div>
    <div class="GeneralDataDiv">
        General data
    </div>
    <form id="form1" runat="server">
        <asp:Table ID="Table1" runat="server" CssClass="DetailsTable">
            <asp:TableHeaderRow CssClass="GeneralDataDiv"></asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Name:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:TextBox ID="tb_Name" runat="server" Enabled="False" CssClass="LongInput TextBox InputControl"></asp:TextBox>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Project:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:DropDownList ID="ddl_Project" runat="server" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="ProjectId" Enabled="False" CssClass="LongInput InputControl">
                    </asp:DropDownList>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                   Country:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:DropDownList ID="ddl_Country" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="CountryId" Enabled="False" CssClass="LongInput InputControl">
                    </asp:DropDownList>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    Start date:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:TextBox ID="tb_StartDate" runat="server" Enabled="False" CssClass="ShortInput TextBox InputControl"></asp:TextBox>
                    <div style="color: red">
                        &nbsp; *
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="TableLabels">
                    End date:
                </asp:TableCell>
                <asp:TableCell CssClass="TableCellcontent">
                    <asp:TextBox ID="tb_EndDate" runat="server" Enabled="False" CssClass="ShortInput TextBox InputControl"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>



        </asp:Table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="SELECT * FROM [Countries]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="SELECT * FROM [Projects]"></asp:SqlDataSource>
        <div style="padding">
            <asp:Label ID="ValidationLabel" runat="server" Visible="false" CssClass="ValidationLabel"></asp:Label>
        </div>
        <div class="ButtonsDiv">
            <asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this Record?');" />
            <asp:Button ID="ButtonEdit" runat="server" OnClick="ButtonEdit_Click" Text="Edit" />
            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False" />
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" Visible="False"/>
            <asp:Button ID="ButtonClose" runat="server" OnClick="ButtonClose_Click" Text="Close" />
        </div>
    </form>
</asp:Content>
