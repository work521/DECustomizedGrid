﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SignalRTest.aspx.cs" Inherits="WebApplication3.SignalRTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <script src="Scripts/jquery-3.6.0.min.js"></script>
    <script src="Scripts/jquery.signalR-2.4.3.min.js"></script>
    <script src="SignalR/hubs"></script>
    <script type="text/javascript">
        $(function () {
            var job = $.connection.MyHub;

            job.client.DisplayStatuc = function () {
                getData();
            };

            $.connection.hub.start();
            getData();
        });
        function getData() {
            var $tbl = $('tbl');
            $.ajax({
                url: 'SignalRTest.aspx/GetData',
                contentType: "applicationjson; charset=utf-8",
                dataType: "json",
                type: "POST",
                success: function (data) {
                    debugger;
                    if (data.d.length > 0) {
                        var newdata = data.d;
                        $tbl.empty();
                        $tbl.append(' <tr><th>Name</th><th>Name</th><th>Name</th><th>Name</th><th>Name</th>');
                        var rows = [];
                        for (var i = 0; i < newdata.length; i++) {
                            rows.push('<tr><td>' + newdata[i].Name + '</td><td>' + newdata[i].Name + '</td><td>' + newdata[i].Name + '</td><td>' + newdata[i].Name + '</td><td>' + newdata[i].Name + '</td>')
                        }
                        $tbl.append(rows.join(''));
                    }
                }
            })
        }
    </script>
</asp:Content>
