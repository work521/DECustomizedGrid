﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using System.Web.Caching;
using System.Web;
using System.Security.Cryptography;
using static WebApplication3.MyEncryptionUtility;
using static WebApplication3.ValidationException;

namespace WebApplication3
{
    public partial class CreateNew : System.Web.UI.Page
    {
        private SqlConnection connection = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=EntityRecords;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddl_Country.DataBind();
                ddl_Project.DataBind();
                SymmetricEncryptionUtility.GenerateKey();
                ddl_Country.Items.Insert(0, new ListItem("Select country", ""));
                ddl_Project.Items.Insert(0, new ListItem("Select project", ""));
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (Valid())
                Save();
        }

        protected bool Valid()
        {
            try
            {
                ValidateControl(tb_Name);
                ValidateControl(ddl_Project);
                ValidateControl(ddl_Country);
                ValidateControl(tb_StartDate);
                ValidateDate(tb_StartDate);
                if (tb_EndDate.Text != "")
                    ValidateDate(tb_EndDate);
            }
            catch (Exception ex)
            {
                
                ValidationLabel.Visible = true;
                ValidationLabel.Text = ex.Message;
                ValidationLabel.BackColor = System.Drawing.Color.FromArgb(166, 44, 41);
                return false;
            }
            return true;
        }

        protected void Save()
        {
            string commandText = "insert into EntityRecords (Name, CountryId, ProjectId, StartDate, EndDate) values (@NAME, @COUNTRYID, @PROJECTID, @STARTDATE, @ENDDATE)";
            SqlCommand command = new SqlCommand(commandText, connection);
            command.Parameters.Add("@NAME", SqlDbType.Text);
            command.Parameters["@NAME"].Value = tb_Name.Text;
            command.Parameters.Add("@COUNTRYID", SqlDbType.Int);
            command.Parameters["@COUNTRYID"].Value = ddl_Country.SelectedValue;
            command.Parameters.Add("@PROJECTID", SqlDbType.Int);
            command.Parameters["@PROJECTID"].Value = ddl_Project.SelectedValue;
            command.Parameters.Add("@STARTDATE", SqlDbType.Date);
            command.Parameters["@STARTDATE"].Value = tb_StartDate.Text;
            command.Parameters.Add("@ENDDATE", SqlDbType.Date);
            if (DateTime.TryParse(tb_EndDate.Text, out _))
            {
                command.Parameters["@ENDDATE"].Value = DateTime.Parse(tb_EndDate.Text);
            }
            else
            {
                command.Parameters["@ENDDATE"].Value = DBNull.Value;
            }
            connection.Open();
            command.ExecuteNonQuery();
            ValidationLabel.Visible = true;
            ValidationLabel.Text = "Record was saved to database";
            ValidationLabel.BackColor = System.Drawing.Color.FromArgb(81, 136, 77);
            byte[] data = SymmetricEncryptionUtility.EncryptData(((int)new SqlCommand("Select top 1 ID from EntityRecords order by ID desc", connection).ExecuteScalar()).ToString());
            Response.Redirect("~/Details.aspx?id=" + Convert.ToBase64String(data), true);
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}