﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication3.Default" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    Records
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <link rel="stylesheet" href="App_Themes/GridViewStyles.css">
    <form id="form1" runat="server">
        <div class="FilterTable">
            Period:<asp:DropDownList ID="DropDownListPeriod" runat="server" AutoPostBack="True" CssClass="filter" Width="15%"
                OnSelectedIndexChanged="DropDownLists_SelectedIndexChanged">
            </asp:DropDownList>
            Country:<asp:DropDownList ID="DropDownListCountry" runat="server" DataSourceID="SqlDSCountries" CssClass="filter" Width="30%"
                DataTextField="COUNTRY_NAME" DataValueField="RECORD_ID" AutoPostBack="True" OnSelectedIndexChanged="DropDownLists_SelectedIndexChanged">
            </asp:DropDownList>
            Project:<asp:DropDownList ID="DropDownListProject" runat="server" DataSourceID="SqlDSProjects" CssClass="filter" Width="30%"
                DataTextField="Name" DataValueField="ProjectId" AutoPostBack="True" OnSelectedIndexChanged="DropDownLists_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:HyperLink runat="server" NavigateUrl="~/CreateNew.aspx" Text="Add new" CssClass="AddButton" />
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" HorizontalAlign="Center" CssClass="table" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDSRecords" OnDataBound="GridView1_DataBound">
            <Columns>
                <asp:TemplateField Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="RecordId" Text='<%#Eval("Id")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" SortExpression="Name" HeaderStyle-CssClass="headers" HeaderStyle-Width="400px">
                    <ItemTemplate>
                        <div class="recordCircle", style="background:var(--leftP<%#Eval("Project_Id")%>),var(--center),var(--rightC<%#Eval("Country_Id")%>)"></div>
                        <asp:HyperLink NavigateUrl='<%#"~/Details.aspx?id="+EncryptID(Convert.ToString(Eval("Id"))) %>' Text='<%#Eval("RECORD_Name")%>' runat="server" CssClass="RecName" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Project" SortExpression="Name1" HeaderStyle-CssClass="headers" HeaderStyle-Width="400px">
                    <ItemTemplate>
                        <asp:Label Text='<%#Eval("Name1")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Start date" SortExpression="StartDate" HeaderStyle-CssClass="headers" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:Label ID="StartDate" Text='<%#Eval("StartDate","{0:dd-MMM-yyyy}")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="End date" SortExpression="EndDate" HeaderStyle-CssClass="headers" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:Label ID="EndDate" Text='<%#Eval("EndDate","{0:dd-MMM-yyyy}")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="CountryId" Text='<%#Eval("CountryId")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="ProjectId" Text='<%#Eval("ProjectId")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Country" SortExpression="Name2" HeaderStyle-CssClass="headers" HeaderStyle-Width="400px">
                    <ItemTemplate>
                        <asp:Label Text='<%#Eval("Name2")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDSCountries" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="SELECT * FROM [Countries]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDSProjects" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="SELECT * FROM [Projects]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDSRecords" runat="server" ConnectionString="<%$ ConnectionStrings:RecordsConnectionString %>" SelectCommand="Select
rec.RECORD_Id,
rec.rECORD_Name,
proj.PROJECT_Name,
Start_Date,
End_Date,
rec.Country_Id,
rec.Project_Id ,
coun.COUNTRY_Name
from Entityrecords  rec
left join Countries coun on rec.Country_Id=coun.RECORD_Id
left join Projects proj on rec.Project_Id=proj.RECORD_Id
            order by end_Date"></asp:SqlDataSource>
    </form>

</asp:Content>