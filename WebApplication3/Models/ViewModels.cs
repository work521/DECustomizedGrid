﻿using System;

namespace WebApplication3.Models
{
    public class CountryViewModel
    {
        public CountryViewModel()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }

    }

    public class ProjectViewModel
    {
        public ProjectViewModel()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ProjectDetails
    {
        public ProjectDetails()
        {
        }

        public string Description { get; set; }
        public int Type { get; set; }
    }
    public class RecordViewModel
    {
        public RecordViewModel()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
    }
    public class CityListItemViewModel
    {
        public CityListItemViewModel()
        {
        }
        public string CountryFlagUNCode { get; set; }
        public int ParentId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class CountryWithRegionViewModel
    {
        public CountryWithRegionViewModel()
        {
        }
        public string FlagUNCode { get; set; }
        public int RegionId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class RegionViewModel
    {
        public RegionViewModel()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public struct FileData
    {
        public FileData(int Id, string Path)
        {
            this.Id = Id;
            this.Path = Path;
        }
        public int Id { get; set; }
        public string Path { get; set; }
    }
}