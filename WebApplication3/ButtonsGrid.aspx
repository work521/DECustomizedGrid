﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ButtonsGrid.aspx.cs" Inherits="WebApplication3.WebForm1" Theme="Skin1" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    Records
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <link rel="stylesheet" href="App_Themes/GridViewStyles.css">
    <script src="Scripts/jquery-3.6.0.js" type="text/javascript"></script>
    <script src="ButtonsScript.js" type="text/javascript"></script>
    <form id="form1" runat="server" enctype="multipart/form-data">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <services>
                <asp:ServiceReference Path="~/WebService.asmx" />
            </services>

        </asp:ScriptManager>

        <input type="button" value="Get data" onclick="BindData()" class="button" />

        <input type="button" value="Get all data" onclick="BindAllData()" class="button" />
        <table id="table" class="table" style="visibility: hidden">
            <thead>
                <tr class="headers">
                    <th style="width: 300px">Name</th>
                    <th style="width: 300px">Project</th>
                    <th style="width: 100px">Start date</th>
                    <th style="width: 100px">End date</th>
                    <th style="width: 300px">Country</th>
                </tr>
            </thead>
            <tbody id="tBody">
            </tbody>
        </table>
        <div style="float: left; margin: 50px">
            <table id="tableCountries" class="table" style="visibility: hidden">
                <thead>
                    <tr class="headers">
                        <th style="width: 100px">Id</th>
                        <th style="width: 300px">Country name</th>
                    </tr>
                </thead>
                <tbody id="tBodyCountries">
                </tbody>
            </table>
        </div>
        <div style="float: right; margin: 50px">
            <table id="tableProjects" class="table" style="visibility: hidden">
                <thead>
                    <tr class="headers">
                        <th style="width: 100px">Id</th>
                        <th style="width: 300px">Project name</th>
                    </tr>
                </thead>
                <tbody id="tBodyProjects">
                </tbody>
            </table>
        </div>
    </form>
</asp:Content>
